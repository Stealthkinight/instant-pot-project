const http = require('http');
const socketIO = require('socket.io');
const express = require('express');

const app = express();


//send data to the one who access localhost 3000 
//app.get('/',(req,res)=>res.send('true'));
app.get('/',(req,res)=>{
	//console.log(req.body);
	var response="connection is successfully set up";
	res.json(response);
});

//when using socketIO,we need passinghttp server
const server = http.Server(app);
server.listen(3000);

//catch the instance of socketIO
const io = socketIO(server);
io.on('connection',(socket)=>{
	socket.on('authenticate user',function(data){
		console.log(data);
		console.log(data.username);
		console.log(data.email);
		if(data.username=='admin' && data.email=='123')
			socket.emit('result',{msg:'true'});
		else
			socket.emit('result',{msg:'false'});
	});	
})