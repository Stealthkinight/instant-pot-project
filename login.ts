import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController} from 'ionic-angular';
import * as socketIO from 'socket.io-client';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage{
    constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {}
    @ViewChild('username') usern;
    @ViewChild('email') em;

	logIn(){
		//set up  connection with server 
		const socket = socketIO('http://localhost:3000');
		//set request to server
		socket.emit('authenticate user',{username:this.usern.value,email:this.em.value});
		socket.on('result',function(e){
			console.log(e);
			document.getElementById('test').innerHTML=JSON.stringify(e);
			//document.getElementById('test').innerHTML=e;
		});

		//console.log(this.usern.value,this.em.value);
		if( this.usern.value=="admin" && this.em.value=='123'){
			this.navCtrl.setRoot('MenuPage');
			//this.navCtrl.push('MenuPage');
		}else{
			//console.log(this.usern.value,this.em.value);
			const alert = this.alertCtrl.create({
		      title: 'Login information is invalid',
		      subTitle: 'Please try again',
		      buttons: ['OK']
		    });
		    alert.present();
		}
		
		/*var test=document.getElementById('test').innerHTML;
		console.log(test);
		console.log(test.indexOf('true')!=-1);
		if(test.indexOf('true')!=-1){
		//if(test.msg){
			//this.navCtrl.setRoot('MenuPage');
			this.navCtrl.push('MenuPage');
		}else{
			//console.log(this.usern.value,this.em.value);
			const alert = this.alertCtrl.create({
		      title: 'Login information is invalid',
		      subTitle: 'Please try again',
		      buttons: ['OK']
		    });
		    alert.present();
		}*/
	}
}
