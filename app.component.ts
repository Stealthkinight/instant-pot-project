//import { Component, ViewChild, OnInit } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import * as socketIO from 'socket.io-client';

import { LoginPage } from '../pages/login/login';
import { AboutPage } from '../pages/about/about';
import { RecipesPage } from '../pages/recipes/recipes';
import { ResourcesPage } from '../pages/resources/resources';
import { SupportPage } from '../pages/support/support';

@Component({
  templateUrl: 'app.html'
})
//export class MyApp implements OnInit{
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = LoginPage;
  activePage:any;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Login', component: LoginPage},
      { title: 'About', component: AboutPage},
      { title: 'Recipes', component: RecipesPage},
      { title: 'Resources', component: ResourcesPage},
      { title: 'Support', component: SupportPage}
    ];
    this.activePage=this.pages[0];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    this.activePage = page;
  }

  checkActive(page){
    return page == this.activePage;
  }

  /*ngOnInit():void{
    const socket = socketIO('http://localhost:3000');
    socket.on('hello',(data)=>console.log(data));
  }*/
}
